const express = require('express')
const mongoose = require('mongoose')
const config = require('config')
const app = express()

const siteRouter = require('./src/routes/site')

app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect(config.mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: false
}).then(() => {
    console.log('Database connected')
}).catch(err => {
    console.log(err)
})
app.use('/public', express.static('public'));
app.use(express.static('public'));
app.use("/",express.static("public/js"))
app.set('view engine', 'ejs');

app.use('/', siteRouter)

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log('successfully running on '+PORT)
})
const express = require('express')
const router = express.Router()

const authenticJWT = require('../middlewares/authenticateJWT')
const loginController = require('../controllers/loginController')
const stockController = require('../controllers/stockController')
const homeController = require('../controllers/homeController')

router.post('/register', loginController.register)
router.post('/login', loginController.login)

router.get('/', homeController.home)

//dropdown api
router.post('/dropdown', stockController.drop_stock)
//search api
router.post('/search', stockController.search)

module.exports = router;
const JWT = require('jsonwebtoken')
const accessTokenSecret = 'youraccesstoken';

const authenticJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];

        JWT.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.json({
                    status: false,
                    message: "Invalid token"
                })
            }
            req.user = user;
            next();
        })
    } else {
        return res.json({
            status: false,
            message: "Invalid token"
        })
    }
}

module.exports = authenticJWT;
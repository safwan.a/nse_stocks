const Users = require('../models/user')

//user register
exports.register = async (req, res) => {
    try {
        let { name, email, password} = req.body
        let user = new Users({
            name,
            email
        })

        user.setPassword(password)
        let token = user.generateAuthToken()
        await user.save()

        return res.json({
            status: true,
            message: "Successfully, registered",
            token: token
        })

    } catch (err) {
        console.log(err)
        return res.json({
            status: false,
            message: "Sorry, something went wrong"
        })
    }
}

//user login
exports.login = async (req, res) => {
    try {
        let {email, password} = req.body;
        let user = await Users.findOne({email: email})

        if (user) {
            if (user.validPassword(password)) {
                let token = user.generateAuthToken()
                return res.json({
                    status: true,
                    message: "Successfully, Login",
                    token: token
                })
            } else {
                return res.json({
                    status: false,
                    message: "Invalid password"
                })
            }
        } else {
            return res.json({
                status: false,
                message: "Can't find user"
            })
        }
    } catch (err) {
        console.log(err)
        return res.json({
            status: false,
            message: "Sorry, something went wrong"
        })
    }
}
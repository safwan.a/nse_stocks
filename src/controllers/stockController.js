const Stocks = require('../models/stocks')

exports.drop_stock = async (req, res) => {
    try {
        console.log('hel')
        let search = req.body.search.trim()
        let regex = new RegExp(".*" + search + ".*", "i");
        var query = [
            {
                $match: {
                    Name: regex
                }
            },
            {
                $project: {
                    Name: 1
                }
            }
        ]

        let company_names = await Stocks.aggregate([query])
        return res.json({
            status: true,
            company_names
        })
    } catch (err) {
        console.log(err)
        return res.json({
            status: false,
            message: "Sorry, something went wrong"
        })
    }
}

//search api
exports.search = async (req, res) => {
    try {
        let {stock_id} = req.body
        let stock = await Stocks.findOne({_id: stock_id})

        if (stock) {
            return res.json({
                status: true,
                stock
            })
        }
    } catch (err) {
        console.log(err)
        return res.json({
            status: false,
            message: "Sorry, something went wrong"
        })
    }
}
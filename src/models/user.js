const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CryptoJs = require('crypto-js');
const JWT = require('jsonwebtoken');
const config = require('config')

const tokensecret = config.JWTKeY;
const nonce = 'usernonce';
const path = 'userPath';
const message = 'userMessage';

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    hash: {
        type: String
    }
}, {timestamps: true})

userSchema.methods.setPassword = function(password) {
    let salt = CryptoJs.SHA256(nonce + password);
    this.hash = CryptoJs.enc.Base64.stringify(CryptoJs.HmacSHA512(path + salt, message))
}

userSchema.methods.validPassword = function(password) {
    let salt = CryptoJs.SHA256(nonce + password);
    let hash = CryptoJs.enc.Base64.stringify(CryptoJs.HmacSHA512(path + salt, message))
    return this.hash === hash
}

userSchema.methods.generateAuthToken = function() {
    let token = JWT.sign({_id: this._id, name: this.name}, tokensecret)
    return token;
}

module.exports = users = mongoose.model('users', userSchema)
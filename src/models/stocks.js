const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const stocksSchema = new Schema({
    company_name: {
        type: String
    },
    current_market_price: {
        type: Number
    },
    market_cap: {
        type: Number
    },
    stock_pe: {
        type: Number
    },
    dividend_yield: {
        type: Number
    },
    roce_percentage: {
        type: Number
    },
    debit_to_equity: {
        type: Number
    },
    eps: {
        type: Number
    },
    reserves: {
        type: Number
    },
    debt: {
        type: Number
    }
}, {timestamps: true})

module.exports = stockes = mongoose.model('stockes', stocksSchema);